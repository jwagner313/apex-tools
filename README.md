
The files in this repository are related to APEX mm-VLBI configuration and station setup,
as well as data inspection.

Newer versions will be at https://github.com/mpifr-vlbi/apex-tools/
